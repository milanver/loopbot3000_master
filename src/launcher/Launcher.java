package launcher;

import controller.MainViewController;
import controller.PlayBarViewController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * @author Micha Lanvers, Leif Niemczik
 */
public class Launcher extends Application {
	private MainViewController mainViewController;

	public static void main(String[] args) {
		endProgramIfAlreadyStarted();
		launch(args);
	}

	@SuppressWarnings("resource")
	private static void endProgramIfAlreadyStarted() {
		try {
			@SuppressWarnings("unused")
			java.net.ServerSocket ss = new java.net.ServerSocket(1359);
		} catch (java.net.BindException ex) {
			System.out.println("Programm läuft bereits.");
			System.exit(1);
		} catch (java.io.IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/MainView.fxml"));
		GridPane root = loader.load();
		mainViewController = loader.getController();
		Scene scene = new Scene(root, 1280, 600);
		mainViewController.determineSize();
		
		addOnCloseRequestHandlerToPrimaryStage(stage);
		stage.setTitle("Loop-Bot 3000");
		stage.getIcons().add(new Image(Launcher.class.getResourceAsStream("/images/icon.png")));
		stage.setResizable(false);
		stage.setScene(scene);
		addFocusListenerToStage(stage, root);
		addKeyEventsToScene(scene, stage);

		stage.show();
	}

	private void addFocusListenerToStage(Stage stage, GridPane root) {
		stage.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (!newValue) {
							root.getStyleClass().add("dimmed");
						} else {
							root.getStyleClass().remove("dimmed");
						}
					}
				});
			}
		});
			
		root.setOnDragEntered(new EventHandler<DragEvent>(){
			@Override
			public void handle(DragEvent event) {
				if(root.getStyleClass().contains("dimmed"))
					root.getStyleClass().remove("dimmed");
			}	
		});
		
		root.setOnDragExited(new EventHandler<DragEvent>(){
			@Override
			public void handle(DragEvent e) {
				if(!root.getStyleClass().contains("dimmed") && !stage.focusedProperty().get()){
					System.out.println("blubb");
					root.getStyleClass().add("dimmed");
				}
			}		
		});		
	}

	private void addOnCloseRequestHandlerToPrimaryStage(Stage stage) {
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	
	private void addKeyEventsToScene(Scene scene, Stage stage){
		scene.setOnKeyPressed(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent event) {
				if(event.isMetaDown() || event.isControlDown()){
					if (event.getCode() == KeyCode.S){
						mainViewController.exportLoopProject();
					}else if(event.getCode() == KeyCode.O){
						mainViewController.importLoopProject();
					} else if(event.getCode() == KeyCode.N){
						mainViewController.openCreateProjectDialog();
					}
				}
			}
		});
		
		scene.addEventFilter(KeyEvent.KEY_PRESSED, event->{
            if (event.getCode() == KeyCode.SPACE) {
            	PlayBarViewController pbc = mainViewController.getPlayBarController();
				pbc.switchPlaying();
				event.consume();
            }
        });
	}
}
